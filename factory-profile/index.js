const app = Vue.createApp({
    data() {
        return {
            profileReports: {}, // 使用物件格式
            start_date: GetFormattedDate(new Date(new Date().getTime() - 7 * 3600 * 24 * 1000)),
            end_date: GetFormattedDate(new Date()),
            creators: [],
            creatorSelected: 'all',
            group_title: '',
            companies: [],
            isLoading: true,
        };
    },
    mounted() {
        console.log("App mounted");
        this.getCompany();
        this.getCreator();
        this.getReport();
    },
    methods: {
        getCreator() {
            return axios.get('./getCreator.php').then(response => {
                this.creators = response.data.creators;
                console.log("Creators fetched:", this.creators);
            }).catch(error => {
                console.error('Error fetching creators:', error);
            });
        },
        getCompany() {
            return axios.get('./getCompany.php').then(response => {
                this.companies = response.data.factorys;
                console.log("Companies fetched:", this.companies);
            }).catch(error => {
                console.error('Error fetching companies:', error);
            });
        },
        getReport() {
            let getReportFile = this.creatorSelected === 'all' ? 'getReportAll.php' : 'getReport.php';
            axios.get(`./${getReportFile}`, {
                params: {
                    start_date: GetROCDateFromAD(this.start_date),
                    end_date: GetROCDateFromAD(this.end_date),
                    creator: this.creatorSelected,
                }
            }).then(response => {
            // 請補足程式碼 

            }).catch(error => {
                console.error('Error fetching report:', error);
                this.isLoading = false;
            });
        },
        // 如需則自行新增fuction 

        UpdateStartDate(value) {
            this.start_date = value;
        },
        UpdateEndDate(value) {
            this.end_date = value;
        },
        UpdateCreator(value) {
            this.creatorSelected = value;
        },
    }
});

app.component("report", {
    template: "#report",
    props: ['start_date', 'end_date', 'report_data', 'group_title', 'companies', 'creators'],
    methods: {
        getReportValue() {
            // 請補足程式碼，可自行加入參數
        },
        getTotalForCompany() {
            // 請補足程式碼，可自行加入參數
        },
        getTotalForCreator() {
            // 請補足程式碼，可自行加入參數
        },
        getGrandTotal() {
            // 請補足程式碼，可自行加入參數
        },
        // 如需則自行新增fuction 
        GetFormattedDate(date) {
            const year = date.getFullYear();
            const month = (date.getMonth() + 1).toString().padStart(2, '0');
            const day = date.getDate().toString().padStart(2, '0');
            return `${year}-${month}-${day}`;
        }
    },
    mounted() {
        console.log("Report component mounted");
        console.log("report comp:", this.report_data);
    },
});

app.component("search", {
    template: "#search",
    props: ['start_date', 'end_date', 'creator_select', 'get_report', 'update_start_date', 'update_end_date', 'update_creator'],
    data() {
        return {
            start_date_text: this.start_date,
            end_date_text: this.end_date,
            creator_selected: this.creatorSelected,
        };
    },
    methods: {
        print() {
            var divToPrint = document.getElementById('report');
            var htmlToPrint = '' +
                '<style type="text/css">' +
                'table {' +
                'border-right: 1px solid #000;' +
                'border-bottom: 1px solid #000;' +
                'border-collapse: collapse;' +
                'text-align:center;' +
                '}' +
                'table td, table th {' +
                'border: 1px solid black;' +
                '}' +
                '</style>';
            htmlToPrint += divToPrint.outerHTML;
            newWin = window.open("");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
        },
        toExcel() {
            var table = document.getElementById('report');
            var workbook = XLSX.utils.table_to_book(table, {
                raw: true // 告訴函數將所有單元格作為原始文本處理
            });

            var excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
            var data = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });

            var url = window.URL.createObjectURL(data);
            var a = document.createElement('a');
            a.href = url;
            let creator = !this.creator_selected || this.creator_selected == 'all' ? '全員' : this.creator_selected;
            a.download = `平台效能評估${this.start_date_text}~${this.end_date_text}${creator}.xlsx`;
            document.body.appendChild(a);
            a.click();

            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        },
    },
    mounted() {
    },
});

app.mount("#app");