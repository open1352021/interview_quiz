<?php
// 需要 php_pdo_sqlsrv_80_ts_x64.dll
try {
    $dbconfig = parse_ini_file('dbconfig.ini', true);
    $server = $dbconfig['connection']['server'];
    $user = $dbconfig['connection']['user'];
    $pwd = $dbconfig['connection']['pwd'];
    $db = $dbconfig['connection']['db'];

    $pdo = new PDO("sqlsrv:Server=$server;Database=$db", "$user", "$pwd");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    die("Connection failed: " . $e->getMessage());
}

?>
