
function GetROCDateFromAD(ADdate) {
    var [year, month, day] = ADdate.split('-');
    year = parseInt(year) - 1911
    return `${year}.${month}.${day}`;
}
function GetFormattedDate(date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
}